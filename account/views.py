import datetime

from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone

from .models import Profile, Account, Action
from .utils import account_generator, message_sender, document_sender, current_dir


def profile(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            return redirect('superadmin:home')
        else:
            profile = get_object_or_404(Profile, user=request.user)
            accounts = Account.objects.filter(leader=profile).order_by('-date_end')
            accounts_count = accounts.count()
            coins = profile.count
            today = timezone.datetime.today().day

            if profile.chat_id == "515098162":
                messages.add_message(request, messages.INFO, 'Please enter your Telegram Chat ID in edit section')

            context = {'profile': profile,
                       'accounts': accounts,
                       'coins': coins,
                       'accounts_count': accounts_count,
                       'today': today,
                       }

            return render(request, 'account/profile.html', context)
    else:
        return redirect('account:login_view')


def check(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('account:profile')
        else:
            messages.add_message(request, messages.INFO, 'Bad Username/Password')
            return redirect('account:login_view')


def login_view(request):
    return render(request, 'account/login.html')


def create_account(request):
    if request.user.is_authenticated:
        profile = get_object_or_404(Profile, user=request.user)
        if profile.count != 0:
            account_name = request.POST['account_name']

            try:
                get_object_or_404(Account, name=account_name)
                messages.add_message(request, messages.INFO, 'This name already taken')
            except:
                if account_name != "":
                    server_ip = profile.server.ir_ip

                    if account_generator(profile, server_ip, account_name):
                        profile.count -= 1
                        profile.save()

                    else:
                        # send notif to admin for charging server
                        messages.add_message(request, messages.INFO,
                                             'There is no raw account on your server, please wait until admin charge it again')
                else:
                    messages.add_message(request, messages.INFO, 'Chose somename and donnot leave it blank !')

    return redirect('account:profile')


def charge_account(request, account_id):
    if request.user.is_authenticated:
        profile = get_object_or_404(Profile, user=request.user)
        if profile.count != 0:
            account = get_object_or_404(Account, id=account_id)
            account.date_end += datetime.timedelta(days=30)
            account.save()

            profile.count -= 1
            profile.save()

            action = Action(leader=get_object_or_404(Profile, user=request.user), action=1, account=account)
            action.save()

            message = '{}: charge > {}'.format(profile, account)
            message_sender(message, '515098162')

        return redirect('account:profile')
    else:
        return redirect('account:profile')


def send_profile(request, account_id):
    if request.user.is_authenticated:
        account = get_object_or_404(Account, id=account_id)

        document_sender(account.leader.chat_id, '{}{}'.format(current_dir, account.file.url), account.password)

        return redirect('account:profile')
    else:
        return redirect('account:profile')


def change_chat_id(request, profile_id):
    profile = get_object_or_404(Profile, pk=profile_id)
    chat_id = request.POST['chat_id']
    print(chat_id)

    profile.chat_id = chat_id
    profile.save()

    return redirect('account:profile')


def actions(request):
    if request.user.is_authenticated:
        leader = get_object_or_404(Profile, user=request.user)
        actions = Action.objects.filter(leader=leader).order_by('date')

        now = timezone.datetime.now()
        context = {'actions': actions, 'now': now}
        return render(request, 'account/actions.html', context)
    else:
        return redirect('account:profile')


def contact(request):
    if request.user.is_authenticated:
        return render(request, 'account/contact.html')
    else:
        return redirect('account:profile')
