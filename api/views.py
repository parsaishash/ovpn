import datetime

from django.shortcuts import get_object_or_404
from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView

from account.models import Account, Server, Profile, Action
from account.utils import account_generator, message_sender
from .serializers import AccountSerializer, ServerSerializer, ProfileSerializer


class HomeView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        content = "You are authenticated"
        return Response(content)


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        token['username'] = user.username

        return token


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


@api_view(['GET'])
def getRoutes(request):
    routes = [
        '/api/token',
        '/api/token/refresh',
    ]

    return Response(routes)


@api_view(['GET', 'POST'])
def getData(request, format=None):
    if request.method == 'GET':
        accounts = Account.objects.all()
        serializer = AccountSerializer(accounts, many=True)
        return Response({'accounts': serializer.data})
    elif request.method == 'POST':
        serializer = ServerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def profile_api(request):
    if request.user.is_superuser:
        return Response({'error': 'You are superuser'}, status=status.HTTP_400_BAD_REQUEST)

    profile = get_object_or_404(Profile, user=request.user)
    accounts = Account.objects.filter(leader=profile).order_by('-date_end')
    accounts_count = accounts.count()
    coins = profile.count
    today = timezone.datetime.today().day

    serialized_profile = ProfileSerializer(profile)
    serialized_accounts = ProfileSerializer(accounts, many=True)

    data = {
        'profile': serialized_profile.data,
        'accounts': serialized_accounts.data,
        'coins': coins,
        'accounts_count': accounts_count,
        'today': today,
    }

    return Response(data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def superadmin_api(request):
    if request.user.is_superuser:
        profiles = Profile.objects.all()
        servers = Server.objects.all()
        accounts = Account.objects.all()

        admin_accounts = Account.objects.filter(leader=get_object_or_404(Profile, user=request.user)).order_by(
            '-date_end')

        serialized_profiles = ProfileSerializer(profiles, many=True)
        serialized_servers = ServerSerializer(servers, many=True)
        serialized_accounts = AccountSerializer(accounts, many=True)
        serialized_admin_accounts = AccountSerializer(admin_accounts, many=True)

        data = {
            'profiles': serialized_profiles.data,
            'servers': serialized_servers.data,
            'accounts': serialized_accounts.data,
            'admin_accounts': serialized_admin_accounts.data,
        }

        return Response(data, status=status.HTTP_200_OK)
    else:
        return Response({'error': 'You are not superuser'}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_account_api(request):
    profile = get_object_or_404(Profile, user=request.user)
    if profile.count != 0:
        account_name = request.data.get('account_name')

        try:
            get_object_or_404(Account, name=account_name)
            return Response({'detail': 'This name is already taken'}, status=status.HTTP_400_BAD_REQUEST)
        except:
            if account_name != "":
                server_ip = profile.server.ir_ip

                if account_generator(profile, server_ip, account_name):
                    profile.count -= 1
                    profile.save()
                    serialized_profile = ProfileSerializer(profile)
                    return Response({'detail': 'Account created successfully', 'profile': serialized_profile.data},
                                    status=status.HTTP_201_CREATED)
                else:
                    # send notification to admin for charging server
                    return Response(
                        {'detail': 'There is no raw account on your server. Please wait until admin charges it again'},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                return Response({'detail': 'Choose a valid account name and do not leave it blank!'},
                                status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuthenticated, IsAdminUser])
def charge_account_api(request):
    profile = get_object_or_404(Profile, user=request.user)
    if profile.count != 0:
        account = get_object_or_404(Account, id=request.GET.get('account_id'))
        account.date_end += datetime.timedelta(days=30)
        account.save()

        profile.count -= 1
        profile.save()

        action = Action(leader=get_object_or_404(Profile, user=request.user), action=1, account=account)
        action.save()

        message = '{}: charge > {}'.format(profile, account)
        # message_sender(message, '515098162')

        return Response({'detail': message}, status=status.HTTP_200_OK)
    return Response(
        {'error': f'profile count is {profile.count} and can not be charged', 'user_id': profile.user_id})
